package rsr;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import net.infonode.docking.RootWindow;
import net.infonode.docking.View;
import net.infonode.docking.properties.RootWindowProperties;
import net.infonode.docking.util.DockingUtil;
import net.infonode.docking.util.PropertiesUtil;
import net.infonode.docking.util.ViewMap;

public class rsr implements ActionListener{
	protected JFrame jFrame;
	
	private JTextArea console;
	protected RootWindow rootWindow;
	private ViewMap viewMap;
	
	// "File" menu buttons
	JMenuItem exitButton;
	
	// "Help" menu buttons
	JMenuItem aboutButton;
	
	public rsr()
	{
		// Setting the frame
		jFrame=new JFrame();
		jFrame.setTitle("RSR");	
		jFrame.setSize(800,600);
		ImageIcon img = new ImageIcon("ressources/icon.png");
		jFrame.setIconImage(img.getImage());
		jFrame.setLocationRelativeTo(null);
		jFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		// Add a listener to ask a confirmation in case of click on the closing button of the frame
		jFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if(JOptionPane.showConfirmDialog(jFrame, "Are you sure ?") == JOptionPane.OK_OPTION){
                    jFrame.setVisible(false);
                    jFrame.dispose();
                }
            }
        });
		
		// Setting the menubar
		JMenuBar menuBar = new JMenuBar();
		
		// Menu "File"
		JMenu fileMenu = new JMenu("File");
		exitButton=new JMenuItem("Exit");    
		exitButton.addActionListener(this);		
		fileMenu.add(exitButton);
		menuBar.add(fileMenu);
		
		// Menu "Help"
		JMenu helpMenu = new JMenu("Help");
		aboutButton = new JMenuItem("about RSR");
		aboutButton.addActionListener(this);
		helpMenu.add(aboutButton);		
		menuBar.add(helpMenu);
				
		jFrame.setJMenuBar(menuBar);

		// DockingWindow Views system
		viewMap=new ViewMap();
		
		console = new JTextArea();
		View consoleView= new View("Console", new ImageIcon("./ressources/consoleIcon.png"), createViewComponent("console"));
		this.viewMap.addView(2, consoleView);
		
		rootWindow=DockingUtil.createRootWindow(viewMap,true);
		
		// Enable title bar style
		RootWindowProperties titleBarStyleProperties = PropertiesUtil.createTitleBarStyleRootWindowProperties();
		rootWindow.getRootWindowProperties().addSuperObject(titleBarStyleProperties);
		
		jFrame.setContentPane(rootWindow);
		
		jFrame.setVisible(true);
		
		print("Software ready\n");
	}

	public static void main(String[] args) {
		@SuppressWarnings("unused")
		rsr r = new rsr();
		
	}
	
	public void actionPerformed(ActionEvent e)
	{    
		if(e.getSource()==exitButton)    
			closed(e);  
		if(e.getSource()==aboutButton)
			about();
	} 
	
	/**
	 * Activated on click on the "about" button in the 
	 * help menu. Shows a panel with information.
	 */
	void about()
	{
		@SuppressWarnings("unused")
		AboutPanel ap= new AboutPanel(); 
	}
	
	
	/**
	 * Function called both by pressing the "x" button on 
	 * the top right corner of the frame or clicking on 
	 * the "exit" button of the "file" menu.
	 * @param ev
	 */
	void closed(ActionEvent ev)
	{
		int confirm = JOptionPane.showOptionDialog(
	             null, "Are You Sure to Close Application?", 
	             "Exit Confirmation", JOptionPane.YES_NO_OPTION, 
	             JOptionPane.QUESTION_MESSAGE, null, null, null);
	    if (confirm == 0) 
	    {
	    	jFrame.dispose();
	    }
	}
	
	private Component createViewComponent(String text)
	{
		if(text.equals("console")) return new JScrollPane(console);
		
		return new JScrollPane(new JTextArea());
	}
	
	/**
	 * Print a text into the console of the main window
	 * @param text
	 */
	void print(String text)
	{
		try
		{
			this.console.append(text);
		}
		catch(Exception e)
		{
			System.out.println("ERROR : unable to print '" + text + "' in console.");
		}
	}
	
}
