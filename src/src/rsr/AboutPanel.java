package rsr;

import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class AboutPanel extends JFrame implements ActionListener
{
	JButton okButton;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AboutPanel()
	{
		new JFrame();
		setTitle("about RSR");
		setSize(450,250);
		setLocationRelativeTo(null);
		setResizable(false);
		
		Container pane = getContentPane();
		JPanel listPane = new JPanel();
		listPane.setLayout(new BoxLayout(listPane, BoxLayout.PAGE_AXIS));
		
		JLabel label = new JLabel("<html><p>RSR (for Road Signs Recognition) is a software designed to detect road signs"
				+ " in a video, using OpenCV library.<br>"
				+ "For more information, see LINK TO INSERT HERE.</p>"
				+ "<h3>Authors :</h3>"
				+ "Shyrle Berrio García - Pontifica Universidad Javeriana, Colombia<br>"
				+ "Nicolas Cointe - Delft University of Technology, The Netherlands</html>");
		label.setAlignmentX(Component.CENTER_ALIGNMENT);
		listPane.add(label);

		okButton= new JButton("OK");
		okButton.addActionListener(this);
		okButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		listPane.add(okButton);
		
		pane.add(listPane);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==okButton)
			dispose();
	}
}
